/**
 * Created by TaF on 08/01/2016.
 */
var myApp = angular.module('myApp', ['ngRoute']);

// configure our routes
myApp.config(function($routeProvider) {
    $routeProvider

    // route for the home page
        .when('/home', {
            templateUrl : '/views/home.html',
            controller  : 'mainController'
        })

        // route for the about page
        .when('/about', {
            templateUrl : '/views/about.html',
            //controller  : 'aboutController'
        })

        // route for the contact page
        .when('/contact', {
            templateUrl : 'views/contact.html',
            //controller  : 'contactController'
        })


        // route for the skills page
        .when('/skills', {
            templateUrl : 'views/skills.html',
            //controller  : 'contactController'
        })


        // route for the projects page
        .when('/projects', {
            templateUrl : 'views/projects.html',
            //controller  : 'projectsController'
        })
        .otherwise({
            redirectTo: '/home'
        })
});
