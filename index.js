/**
 * Created by TaF on 08/01/2016.
 */

var express = require("express")
    , bodyParser = require("body-parser")
    , session = require("cookie-session");



var app = express();


// load route.
require('./app/routes.js')(app); // load our routes and pass in our app and

// set app setting

app.use(bodyParser()); // get information from html forms
app.use(express.static(__dirname + '/public/myApp'));                 // set the static files location /public/img will be /img for users
app.use(express.static(__dirname + '/node_modules'));                 // set the static files location /nodes_modules
app.use(bodyParser.urlencoded({'extended':'true'}));            // parse application/x-www-form-urlencoded
app.use(bodyParser.json());                                     // parse application/json
app.use(bodyParser.json({ type: 'application/vnd.api+json' })); // parse application/vnd.api+json as json



// Launch ======================================================================
// listen (start app with node server.js) ======================================
app.listen(8080);
console.log("App listening on port 8080");
